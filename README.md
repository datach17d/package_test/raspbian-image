# Raspbian Image Builder using Packer

 This repository contains the packer `json` to build a Raspberry PI image with the `mini_package.deb` package installed.

 There is also a [`docker-compose`](gitlab-runner/docker-compose.yaml) that can be used in combination with the `.gitlab-ci.yaml` to automatically build an image.

## Local build

 To perform a local build docker must be installed and your used added to the `docker` group.

 The image `registry.gitlab.com/datach17d/package_test/packer-builder-arm:v0.0.2` requires a `docker login` to gitlab.com to be performed 
 before it can be used:

    docker login gitlab.com -u <username> -p <token>

 Note: you can use either a token or your gitlab password (2factor complicates using a password, so a token in prefered).
 The token can be obtained by visting your profile -> Preferences -> [Access tokens](https://gitlab.com/-/profile/personal_access_tokens)
 and creating a `read_api` token - see the [official documentation](https://docs.gitlab.com/ee/user/packages/container_registry/#authenticate-with-the-container-registry).

 Obtain version of the `deb` package `mini_package.deb` from [here](https://gitlab.com/datach17d/package_test/mini-package/-/packages).
 The file must in the same directory as [`rasbian.json`](raspbian.json).

 Run the script [`build_image_local.sh`](build_image_local.sh) to start a docker container that builds `raspberry-pi.img`:

    $ ./build_image_local.sh
    uname -a:  Linux cec1abb26497 5.11.0-36-generic #40~20.04.1-Ubuntu SMP Sat Sep 18 02:14:19 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
    {
    "supported": [
        "linux/amd64",
        "linux/arm64",
        "linux/riscv64",
        "linux/ppc64le",
        "linux/s390x",
        "linux/386",
        "linux/mips64le",
        "linux/mips64",
        "linux/arm/v7",
        "linux/arm/v6"
    ],
    "emulators": [
        "jar",
        "python2.7",
        "python3.8",
        "qemu-aarch64",
        "qemu-alpha",
        "qemu-arm",
        "qemu-armeb",
        "qemu-cris",
        "qemu-hppa",
        "qemu-m68k",
        "qemu-microblaze",
        "qemu-mips",
        "qemu-mips64",
        "qemu-mips64el",
        "qemu-mipsel",
        "qemu-mipsn32",
        "qemu-mipsn32el",
        "qemu-ppc",
        "qemu-ppc64",
        "qemu-ppc64abi32",
        "qemu-ppc64le",
        "qemu-riscv32",
        "qemu-riscv64",
        "qemu-s390x",
        "qemu-sh4",
        "qemu-sh4eb",
        "qemu-sparc",
        "qemu-sparc32plus",
        "qemu-sparc64",
        "qemu-xtensa",
        "qemu-xtensaeb"
    ]
    }
    running /bin/packer build -debug raspbian.json
    Debug mode enabled. Builds will not be parallelized.
    arm: output will be in this color.

    ==> arm: Retrieving rootfs_archive
    ==> arm: Trying https://downloads.raspberrypi.org/raspbian/images/raspbian-2020-02-14/2020-02-13-raspbian-buster.zip?archive=false
    ==> arm: Trying https://downloads.raspberrypi.org/raspbian/images/raspbian-2020-02-14/2020-02-13-raspbian-buster.zip?archive=false&checksum=file%3Ahttps%3A%2F%2Fdownloads.raspberrypi.org%2Fraspbian%2Fimages%2Fraspbian-2020-02-14%2F2020-02-13-raspbian-buster.zip.sha256
    ==> arm: https://downloads.raspberrypi.org/raspbian/images/raspbian-2020-02-14/2020-02-13-raspbian-buster.zip?archive=false&checksum=file%3Ahttps%3A%2F%2Fdownloads.raspberrypi.org%2Fraspbian%2Fimages%2Fraspbian-2020-02-14%2F2020-02-13-raspbian-buster.zip.sha256 => /build/packer_cache/b678dbc5a0f2979aea35b2fb9efb4543759e1043.zip
        arm: unpacking /build/packer_cache/b678dbc5a0f2979aea35b2fb9efb4543759e1043.zip to raspberry-pi.img
        arm: searching for empty loop device (to map raspberry-pi.img)
        arm: mapping image raspberry-pi.img to /dev/loop4
        arm: mounting /dev/loop4p2 to /tmp/936461512
        arm: mounting /dev/loop4p1 to /tmp/936461512/boot
        arm: running extra setup
        arm: mounting /dev with: [mount --bind /dev /tmp/936461512/dev]
        arm: mounting /devpts with: [mount -t devpts /devpts /tmp/936461512/dev/pts]
        arm: mounting proc with: [mount -t proc proc /tmp/936461512/proc]
        arm: mounting binfmt_misc with: [mount -t binfmt_misc binfmt_misc /tmp/936461512/proc/sys/fs/binfmt_misc]
        arm: mounting sysfs with: [mount -t sysfs sysfs /tmp/936461512/sys]
        arm: running the provision hook
    ==> arm: Pausing before the next provisioner . Press enter to continue. 
    ==> arm: Uploading mini_package.deb => /tmp/mini_package.deb
        arm: mini_package.deb 3.20 KiB / 3.20 KiB [============================================================] 100.00% 0s
    ==> arm: Pausing before the next provisioner . Press enter to continue. 
    ==> arm: Provisioning with shell script: /tmp/packer-shell672630657
        arm: Selecting previously unselected package mini_package.
        arm: (Reading database ... 93514 files and directories currently installed.)
        arm: Preparing to unpack /tmp/mini_package.deb ...
        arm: Unpacking mini_package (0.1.1) ...
        arm: Setting up mini_package (0.1.1) ...
        arm: optional (please ignore) `fuser -k` failed with exit status 1:
    Build 'arm' finished after 1 minute 16 seconds.

    ==> Wait completed after 1 minute 16 seconds

    ==> Builds finished. The artifacts of successful builds are:
    --> arm: raspberry-pi.img


## Gitlab Pipeline Build

 The `docker-compose` and setup script under [`gitlab-runner/`](gitlab-runner/) sets up the runner to have a the correct parameters to perform an automatic build.

 CAUTION: Packer requires a privilleged container with access to the `/dev` directory on the host - this runner should only be used on
 protected branches by trusted users due to it's heightened privilleges.

### Setup

 Start the `docker-compose`:

    $ cd gitlab-runner
    $ docker-compose up -d

 Note: the configuration for the runner will be under `gitlab-runner/config/config.toml`.

 To register the runner with project (and generate the `config.tom`) use the following:

    $ ./register_runner.sh <token>

 The `<token>` can be obtained from the project -> Settings -> CI/CD -> Runners under the heading "Sepecific runners"

### Triggering build

 TODO