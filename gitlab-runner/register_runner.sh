#!/bin/bash

docker-compose exec gitlab-runner gitlab-runner register \
        --non-interactive \
        --url "https://gitlab.com/" \
        --registration-token "$1" \
        --description "image-builder" \
        --executor "docker" \
        --docker-privileged=true \
        --docker-image ubuntu:20.04 \
        --docker-volumes "/dev:/dev" \
        --docker-volumes "/cache" \
        --docker-cache-dir "/cache" \
        --locked=true \
        --access-level="ref_protected"

